<!doctype html>
<html lang="ua">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task1</title>
</head>
<body>
<?php
function my_tg($a)
{
    // тангенс х
    return sin($a) / cos($a);
}

function factorial($a)
{
    if ($a == 0){
        return 1;
    }else{
        return $a * factorial($a - 1);
    }
}


function power_x($a, $b)
{
    for($i = 1; $i < $b; $i++)
    {
        $a *= $a;
    }
    return $a;
}

function average($a,$b)
{
    return ($a + $b) / 2;
}


?>
<style>
    table,tr,td{
        border: 1px solid black;
        border-collapse: collapse;

    }

    .capt td{
        background-color: yellow;
        font-style: oblique;
        font-size: 20px;
    }
</style>
<table>
    <tr class="capt">
        <td>x^y</td><td>x!</td><td>my_tg(x)</td><td>sin(x)</td><td>cos(x)</td><td>tg(x)</td><td>x+y</td><td>x-y</td><td>x*y</td><td>x/y</td><td>average(x,y)</td>
    </tr>
    <tr>
        <?php

        if (isset($_POST['sub']))
        {
            $x = $_POST['x'];
            $y = $_POST['y'];

            $pow = power_x($x, $y);
            $fact = factorial($x);
            $mtg = my_tg($x);
            $snx = sin($x);
            $csx = cos($x);
            $tgx = tan($x);
            $sum = $x + $y;
            $sub = $x - $y;
            $mul = $x * $y;
            $div = $x / $y;
            $avr = average($x, $y);


            echo "<td>$pow</td>  <td>$fact</td>  <td>$mtg</td>  <td>$snx</td>
                  <td>$csx</td>  <td>$tgx</td>  <td>$sum</td>  <td>$sub</td>  
                  <td>$mul</td>  <td>$div</td>  <td>$avr</td>";
        }
        ?>

    </tr>
</table>
<form method="post" action="Task1.php">
    <label>
        <input type="text" name='x' value='<?= $x?>'>
    </label>
    <label>
        <input type="text" name='y' value='<?= $y?>'>
    </label>
    <input type="submit" name='sub'>
</form>
</body>
</html>

