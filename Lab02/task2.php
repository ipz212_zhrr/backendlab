<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task2</title>
</head>
<body>
<style>
    table,tr,td{
        border: 1px solid black;
        border-collapse: collapse;
    }

    td{
        padding: 20px 20px 20px 20px;
    }

</style>
<?php
    function random_html_color()
    {
        return sprintf( '#%02X%02X%02X', rand(0, 255), rand(0, 255), rand(0, 255) );
    }

    function draw_table($n){
        echo "<div>";
        echo '<table>';
        for($i = 0;$i < $n;$i++)
        {
            echo '<tr>';
            for($j = 0;$j < $n;$j ++)
            {
                echo '<td style="background-color:'.random_html_color().';">';
                echo '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
        echo "</div>";
    }

    function random_square($n){
        echo "<style>
              body{
               background-color: black;
              }
              </style>";

        for($i = 0; $i < $n; $i++ )
        {
            $a = rand(50, 400);
            $x = rand(0,800);
            $y = rand(0,900);

            echo
            '<div 
                style="background-color: red;
                       border:solid yellow 2px ;
                       width:'.$a.'px;
                       height:'.$a.'px;
                       margin-left:'.$x.'px;
                       margin-top:'.$y.'px;
                       position: absolute;
                       ">
                
            </div>';
        }
    }


    if(isset($_POST['table']))
        draw_table(rand(5,20));

    if(isset($_POST['square']))
        random_square(rand(1,20));
?>
<form method="POST">
    <button name="table" type="submit" value="1">1</button>
    <button name="square" type="submit" value="2">2</button>
</form>

</body>
</html>
