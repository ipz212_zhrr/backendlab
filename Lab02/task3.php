<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task3</title>
</head>
<body>
<?php
    function count_elem($array)
    {
        sort($array);
        for($i = 0;$i < count($array);$i++){
            $counter = 0;
            if($array[$i] == $array[$i+1])
            {
                continue;
            }
            for ($j = 0;$j < count($array); $j++){
                if($array[$i] == $array[$j]){
                    $counter++;
                }
            }
            echo '<p>['.$array[$i].'] => '.$counter.'</p> ';
        }
    }


    function pet_name_generator($arr_sym){
        $len = rand(2,4);
        echo '<div>';
        for($i = 0; $i < $len;$i++)
        {
            $key = rand(0,count($arr_sym)-1);
            echo "$arr_sym[$key]";
        }
        echo '</div>';

    }


    function createArray()
    {
        $array = array();
        $len = rand(3, 7);
        for ($i = 0; $i < $len; $i++) {
            $array[]=rand(10,20);
        }
        return $array;
    }


    function combine_arrays($arr1,$arr2)
    {
        $unite_arr = [];
        foreach ($arr1 as $item){
            $unite_arr[] = $item;
        }
        foreach ($arr2 as $item){
            $unite_arr[] = $item;
        }
        sort($unite_arr);
        return $unite_arr;
    }


    function clear_arr($arr){
        $clear_array = [];
        for($i = 0;$i < count($arr);$i++){
            if($arr[$i] == $arr[$i+1]){
                continue;
            }
            $clear_array[] = $arr[$i];
        }
        return $clear_array;
    }


    function unite_function(){
        print_r(
            clear_arr(
                combine_arrays(
                    createArray(),
                    createArray()
                )
            )
        );
    }

    if(isset($_POST['count_arr']))
        count_elem(createArray());

    if (isset($_POST['pet_name']))
        pet_name_generator(['на','су','сі','ка','со','тер','фе','ля','бо','му','чик','а']);

    if(isset($_POST['arr_operations']))
        unite_function();

?>
<form method="POST">
    <button name="count_arr" type="submit" value="1">1</button>
    <button name="pet_name" type="submit" value="2">2</button>
    <button name="arr_operations" type="submit" value="3">3</button>
</form>
</body>
</html>
