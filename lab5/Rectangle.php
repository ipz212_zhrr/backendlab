<?php

class Rectangle
{
    static $id_next = 1;

    public $angle_x, $angle_y, $width, $height;
    private $id;

    function __construct($x, $y, $width, $height)
    {
        $this->angle_x = $x;
        $this->angle_y = $y;
        $this->width = $width;
        $this->height = $height;
        $this->setId($this::$id_next);
        $this::$id_next++;
    }
    public function setId($id){
        if (is_integer($id))
            $this->id = $id;
        else
            echo "Неправильне значення ідентифікатора! Введіть значення типу int!";
    }
    public function getId(){
        return $this->id;
    }

    function __clone(){
        $this::$id_next++;
        $this->setId($this::$id_next-1);
    }
}

function printId($rect){
    echo "id = {$rect->getId()}<br>";
    echo "next id = {$rect::$id_next}<br>";
}

$rect1 = new Rectangle(-5, 2, 6, 4);
echo "rectangle 1:<br>";
printId($rect1);
$rect2 = new Rectangle(1, 3, 12, 6);
echo "<br>rectangle 2:<br>";
printId($rect2);

$rect3 = clone $rect1;
echo "<br>rectangle3 (clone rectangle1)<br>";
printId($rect3);

$rect4 = clone $rect2;
echo "<br>rectangle4 (clone rectangle2)<br>";
printId($rect4);