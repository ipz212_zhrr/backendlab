<?php

class actionWithFiles
{
    static $dir = "text";

    static function ReadFile($fileName){
        $file = pathinfo($fileName);
        $str = $file['filename'];
        echo "<b>Method:</b> ReadFile  <b>File:</b> $str<br>";
        $fd = fopen($fileName, "r") or die ("Could not open a file");
        $str = htmlentities(file_get_contents($fileName));
        fclose($fd);
        echo $str;
        echo "<br>";
    }

    static function WriteToFile($fileName, $text){
        $file = pathinfo($fileName);
        $str = $file['filename'];
        echo "<b>Method:</b> WriteToFile <b>File:</b> $str<br>";
        $s = htmlentities(file_get_contents($fileName));
        echo "Before: $s";
        echo "<br>";
        $fd = fopen($fileName, "a");
        fwrite($fd, $text);
        $s = htmlentities(file_get_contents($fileName));
        echo "After: $s";
        fclose($fd);
        echo "<br>";

    }
    function RemoveAllFromFile($fileName){
        $file = pathinfo($fileName);
        $str = $file['filename'];
        echo "<b>Method:</b> RemoveAllFromFile  <b>File:</b> $str<br>";
        $fd = fopen($fileName, "w");
        fwrite($fd, "");
        fclose($fd);
        echo "From $str removed all data";
    }
}

$file = new actionWithFiles();
$dir = $file::$dir;
$file::ReadFile("$dir/file1.txt");

$file::WriteToFile("$dir/file2.txt", "how are you??");

$file->RemoveAllFromFile("$dir/file3.txt");
