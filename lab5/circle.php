<?php

class Circle {
    private $center_x, $center_y, $radius;

    function __construct($center_x, $center_y,$radius){
        $this->center_x = $center_x;
        $this->center_y = $center_y;
        $this->radius = $radius;
    }
    function toString(){
        return "Коло з центром в ($this->center_x, $this->center_y) і радіусом $this->radius";
    }
    function getCenterX(){
        return $this->center_x;
    }
    function getCenterY(){
        return $this->center_y;
    }
    function getRadius(){
        return $this->radius;
    }

    function setCenterX($centerX){
        $this->center_x = $centerX;
    }
    function setCenterY($centerY){
        $this->center_y = $centerY;
    }
    function setRadius($radius){
        $this->radius = $radius;
    }
    function isCirclesIntersect($otherCircle){
        $sum = $otherCircle->getRadius() + $this->radius;
        echo $sum;
        $len = sqrt(pow(($otherCircle->getCenterX()-$this->center_x),2) + pow(($otherCircle->getCenterY()-$this->center_y), 2));
        if ($len>$sum)
            return "false";
        return "true";
    }
}

$circle = new Circle(1,-5,4);
echo $circle->toString();
echo "<br>";
$circle->setCenterY(5);
$circle->setRadius(1);
echo "radius = {$circle->getRadius()}";
echo "<br>";
echo $circle->toString();

$circle1 = new Circle(8,5,4);
echo "<br>bool = {$circle1->isCirclesIntersect($circle)}";
