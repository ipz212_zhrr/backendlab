<?php

namespace task_5;

class Student extends Human
{
    protected $university_name, $course, $group_name;

    function __construct($name, $sname, $age, $weight, $height, $unName, $course, $groupName)
    {
        parent::__construct($name, $sname, $age, $weight, $height);
        $this->university_name = $unName;
        $this->course = $course;
        $this->group_name = $groupName;
    }

    function getUniversityName(){
        return $this->university_name;
    }
    function getYearOfStudy(){
        return $this->course;
    }
    function getGroupName(){
        return $this->group_name;
    }

    function setUniversityName($unName){
        $this->university_name = $unName;
    }
    function setYearOfStudy($course){
        $this->course = $course;
    }
    function setGroupName($groupName){
        $this->group_name = $groupName;
    }

    function TransferTheStudentToANewCourse(){
        $this->course++;
    }

    protected function MessageAtTheBirthOfAChild()
    {
        echo "A student was born!<br>";
    }

    function RoomCleaning()
    {
        return "Студент прибирає кімнату<br>";
    }

    function KitchenCleaning()
    {
        return "Студент прибирає кухню<br>";
    }
}

