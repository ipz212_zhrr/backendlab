<?php

namespace task_5;

abstract class Human implements IHouseCleaning
{
    private $name, $surname ,$age, $weight, $height;

    function __construct($name, $sname, $age, $weight, $height){
        $this->name = $name;
        $this->surname = $sname;
        $this->age = $age;
        $this->weight = $weight;
        $this->height = $height;
    }

    function getName(){
        return $this->name;
    }
    function getSurname(){
        return $this->surname;
    }
    function getAge(){
        return $this->age;
    }
    function getWeight(){
        return $this->weight;
    }
    function getHeight(){
        return $this->height;
    }

    function setName($name){
        $this->name = $name;
    }
    function setSurname($sname){
        $this->surname = $sname;
    }
    function setAge($age){
        $this->age = $age;
    }
    function setWeight($weight){
        $this->weight = $weight;
    }
    function setHeight($height){
        $this->height = $height;
    }
    protected abstract function MessageAtTheBirthOfAChild();

    function BirthOfAChild(){
        $this->MessageAtTheBirthOfAChild();
    }
}

interface IHouseCleaning{
    function RoomCleaning();
    function KitchenCleaning();
}