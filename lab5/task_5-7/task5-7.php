<?php
include "Human.php";
include "Student.php";
include "Programmer.php";
use task_5\Human as Human;
use task_5\Student as Student;
use task_5\Programmer as Programmer;

/*$human = new Human("Lesia", "Rokytna", 20, 54, 165);
echo "Human: Name: {$human->getName()}; Surname: {$human->getSurname()}; Age: {$human->getAge()}";
echo "<br>-----------<br>";*/

$student = new Student("Anastasia", "Til", 19, 50, 170, "ЖДТУ", 2, "ІПЗ-21-6");
$student->BirthOfAChild();
echo "Student: Name: {$student->getName()}; Surname: {$student->getSurname()}; 
    Age: {$student->getAge()}; Course: {$student->getYearOfStudy()}; Group name: {$student->getGroupName()}";
echo "<br>Виклик метода TransferTheStudentToANewCourse()";
$student->TransferTheStudentToANewCourse();
echo "<br>Course: {$student->getYearOfStudy()}";
echo $student->KitchenCleaning();
echo $student->RoomCleaning();
echo "<br>-----------<br>";


$programmer = new Programmer("Oleksiy", "Koval", 22, 65, 175, 2, "C", "C++", "C#", "Python");
$programmer->BirthOfAChild();
echo "Student: Name: {$programmer->getName()}; Surname: {$programmer->getSurname()}; 
    Age: {$programmer->getAge()}; Height: {$programmer->getHeight()} Experience: {$programmer->getExperience()};";
$programmer->PrintLang();

echo "<br>Виклик метода AddLanguage()";

$programmer->AddLanguage("Java");
$programmer->PrintLang();
echo $programmer->KitchenCleaning();
echo $programmer->RoomCleaning();


