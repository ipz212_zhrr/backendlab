<?php

namespace task_5;

class Programmer extends Human
{
    protected $languages = [], $experience;

    function __construct($name, $sname, $age, $weight, $height, $exp, ...$lang)
    {
        parent::__construct($name, $sname, $age, $weight, $height);
        $this->languages = $lang;
        $this->experience = $exp;
    }

    function getLanguages(){
        return $this->languages;
    }
    function getExperience(){
        return $this->experience;
    }
    function setLanguages(...$lang){
        $this->languages = $lang;
    }
    function setExperience($exp){
        $this->experience = $exp;
    }

    function AddLanguage($language){
        $this->languages[] = $language;
    }

    function PrintLang(){
        echo "<br>Languages: ";
        foreach ($this->languages as $item)
            echo "$item,";
    }

    protected function MessageAtTheBirthOfAChild()
    {
        echo "A programmer was born!<br>";
    }

    function RoomCleaning()
    {
       return "Програміст прибирає кімнату<br>";
    }

    function KitchenCleaning()
    {
        return "Програміст прибирає кухню<br>";
    }
}