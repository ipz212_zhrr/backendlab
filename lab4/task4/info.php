<?php
session_start();
$_SESSION['login'] = $_POST['login'];
$_SESSION['password'] = $_POST['password'];
$_SESSION['password2'] = $_POST['password2'];
$_SESSION['sex'] = $_POST['sex'];
$_SESSION['city'] = $_POST['city'];
$_SESSION['games'] = $_POST['games'];
$_SESSION['about'] = $_POST['about'];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    td#s {
        color: gray;
        font-size: 14px;
    }
    img {
        height: 200px;
    }
    a {
        font-size: 14px;
    }
    td {
        padding-bottom: 15px;
    }
</style>
<body>
<?php
$login = $_POST['login'];
$password = $_POST['password'];
$password2 = $_POST['password2'];
if($password != $password2) {
    $password = strlen($password);
    $password2 = strlen($password2);
    $password1 = "не співпадає (перший - $password символів, другий - $password2 символів)";
}
else{
    $password1 = "співпадає";
}
$sex = $_POST['sex'];
$city = $_POST['city'];
$games = $_POST['games'];
$about = $_POST['about'];

$uploads_dir = 'C:/php/upload/';
$img = $_FILES['img']['name'];
$uploads_dir .= $img;

?>
<table>
    <tr>
        <td id="s">Логін:</td>
        <td><?=$login?></td>
    </tr>
    <tr>
        <td id="s">Пароль:</td>
        <td><?=$password1?></td>
    </tr>
    <tr>
        <td id="s">Стать:</td>
        <td><?=$sex?></td>
    </tr>
    <tr>
        <td id="s">Місто:</td>
        <td><?=$city?></td>
    </tr>
    <tr>
        <td id="s">Улюблені ігри:</td>
        <td><?=implode("<br>", $games);?></td>
    </tr>
    <tr>
        <td id="s">Про себе:</td>
        <td><?=$about?></td>
    </tr>
    <tr>
        <td id="s">Фотографія:</td>
        <td><img src="<?=$uploads_dir ?>"></td>
    </tr>
</table>
<a href="index.php">Повернутися на головну сторінку</a>

</body>
</html>