<?php
session_start();
if (isset($_GET['lang'])) {
    $lang = $_GET['lang'];
    setcookie('lang', $lang);
}
if (isset($_COOKIE['lang'])){
$langName = $_COOKIE['lang'];
if ($langName == 'ukr'){
    $langName = "Українська";
}
elseif ($langName == 'eng'){
    $langName = "Англійська";
}
elseif($langName == 'fra'){
    $langName = "Французька";
}
elseif ($langName == 'ger') {
    $langName = "Німецька";
}
    $selectLang = $langName;
}
else{
    $selectLang = "Українська";
}

//session_destroy();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        img
        {
            width: 30px;
            height: 15px;
        }
        p#lan{
            font-size: 14px;
        }
    </style>
</head>
<body>
<form action="" method="get">
    <a href="index.php?lang=ukr" ><img src="uploads/ukr.png"></a>
    <a href="index.php?lang=eng" ><img src="uploads/eng.jpg"></a>
    <a href="index.php?lang=fra" ><img src="uploads/fra.png"></a>
    <a href="index.php?lang=ger" ><img src="uploads/ger.png"></a>
    <p id="lan">Вибрана мова: <?=$selectLang?></p>

</form>
<form action="info.php" enctype="multipart/form-data" method="post">
    <table>
        <tr>
            <td>Логін:</td>
            <td>
                <input type="text" name="login" value="<?=$_SESSION['login']?>"/>
            </td>
        </tr>
        <tr>
            <td>Пароль:</td>
            <td>
                <input type="password" name="password" value="<?=$_SESSION['password']?>"/>
            </td>
        </tr>
        <tr>
            <td>Пароль (ще раз):</td>
            <td>
                <input type="password" name="password2" value="<?=$_SESSION['password2']?>"/>
            </td>
        </tr>
        <tr>
            <td>Місто:</td>
            <td>
                <select name="city" ">
                    <option value="Житомир" <?php if($_SESSION['city'] == 'Житомир') {echo"selected";}?> >Житомир</option>
                    <option value="Київ" <?php if($_SESSION['city'] == 'Київ') {echo"selected";}?>>Київ</option>
                    <option value="Львів" <?php if($_SESSION['city'] == 'Львів') {echo"selected";}?>>Львів</option>
                    <option value="Харків" <?php if($_SESSION['city'] == 'Харків') {echo"selected";}?>>Харків</option>
                    <option value="Одеса" <?php if($_SESSION['city'] == 'Одеса') {echo"selected";}?>>Одеса</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Стать:</td>
            <td>
                <div>
                    <input type="radio" id="sexChoice1"
                           name="sex" value="чоловік" <?php if($_SESSION['sex'] == 'чоловік') {echo"checked";}?>>
                    <label for="sexChoice1">чоловік</label>

                    <input type="radio" id="sexChoice2"
                           name="sex" value="жінка" checked>
                    <label for="sexChoice2">жінка</label>
                </div>
            </td>
        </tr>
        <tr>
            <td>Улюблені ігри:</td>
            <td>
                <?php if($_SESSION['games']==null) $_SESSION['games']=[]?>
                <input type="checkbox" id="g1" name="games[]" value="футбол" <?php if(in_array("футбол",$_SESSION["games"])) echo "checked"?>>
                <label for="g1">футбол</label><br>
                <input type="checkbox" id="g2" name="games[]" value="баскетбол" <?php if(in_array("баскетбол",$_SESSION["games"])) echo "checked"?>>
                <label for="g2">баскетбол</label><br>
                <input type="checkbox" id="g3" name="games[]" value="волейбол" <?php if(in_array("волейбол",$_SESSION["games"])) echo "checked"?>>
                <label for="g3">волейбол</label><br>
                <input type="checkbox" id="g4" name="games[]" value="шахи" <?php if(in_array("шахи",$_SESSION["games"])) echo "checked"?>>
                <label for="g4">шахи</label><br>
                <input type="checkbox" id="g5" name="games[]" value="настільний теніс" <?php if(in_array("настільний теніс",$_SESSION["games"])) echo "checked"?>>
                <label for="g5">настільний теніс</label>

            </td>
        </tr>
        <tr>
            <td>Про себе:</td>
            <td>
                <textarea style="height: 150px" name="about"><?=$_SESSION['about']?></textarea>
            </td>
        </tr>
        <tr>
            <td>Фотографія:</td>
            <td>
                <input type="file" accept="image/png, image/jpeg, image/jpg" name="img">
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="button" value="Зареєструватися"></td>
        </tr>
    </table>
</form>
</body>
</html>