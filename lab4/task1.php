<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
if (isset($_POST['sub'])) {
    if (isset ($_POST['text'])) {
        $text = htmlentities($_POST['text']);
        $exp = '/(http|https):\/\/(-\.)?([^\s\/?\.]+\.?)+(\/[^\s]*[^.\s])/i';
        $res = preg_match_all($exp, $text, $matches);
        $match = [];
        foreach ($matches[0] as $el) {
            echo "$el ";
            $match[] = $el;
        }
        echo "<br>Кількість входжень = $res <br>";
        $str = preg_replace($exp, "тут була адреса сайту ", $text);
        echo "<br><br>$str";
    }
}
?>
<form action="" method="post">
    <p>Text:</p>
    <textarea name="text" cols="40" rows="10"></textarea>
    <input type="submit" name="sub">
</form>
</body>
</html>
<!--про регулярні вирази в php: https://www.w3schools.com/php/php_ref_regex.asp
опис функції scandir(): https://www.php.net/manual/ru/function.scandir.php
курс backend: https://learn.ztu.edu.ua/course/view.php?id=4750-->
