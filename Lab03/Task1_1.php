<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task1.1</title>
</head>
<body>
<form method="post" action="Task1_1.php">
    <label>Текст:
        <input type="text" name="text" value="<?= $text ?>">
    </label><br><br>
    <label>Знайти:
        <input type="text" name="find" value="<?= $fnd ?>">
    </label><br><br>
    <label>Замітити на:
        <input type="text" name="replace" value="<?= $rep ?>">
    </label>
        <input type="submit" name="sub" value="Замінити"><br><br>
    <label>Результат:

        <?php
        if (isset($_POST['sub']))
        {
            $text = $_POST['text'];
            $fnd = $_POST['find'];
            $rep = $_POST['replace'];

            $text = str_replace($fnd, $rep, $text);

            echo '<textarea rows="5" cols="14">'.$text.'</textarea>';
        }
        ?>
    </label>
</form>
</body>
</html>
