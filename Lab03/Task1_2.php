<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task1.2</title>
</head>
<body>
    <form method="post" action="Task1_2.php">
        <label>Введіть список міст
            <input type="text" name="cities">
        </label>
        <br>
        <input type="submit" name="sub" value="Сортувати">
        <br>
        <label>
            <?php
                if(isset($_POST['sub']))
                {
                  $cities = $_POST['cities'];

                  $city_arr = explode(' ', $cities) ;
                  array_multisort($city_arr);

                  echo implode(' ', $city_arr);
                }
            ?>
        </label>
    </form>
</body>
</html>
