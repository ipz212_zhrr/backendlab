<!doctype html>
<html lang="uk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WorkWithFile</title>
    <style>
        textarea{
            width: 500px;
            height: 100px;
        }

        table{
            border-collapse: collapse;
            width: 500px;
        }
        td{
            border:1px solid black;
        }
    </style>
</head>
<body>
<?php
$comment[] = ["name" => 'Автор', "comment" => 'Коментар'];
?>
<form method="post" >
    <label>Ім'я:<br>
        <input type="text" name="name">
    </label>
    <input type="submit" name="sub">
    <br>
    <label>Коментар:<br>
        <textarea name="commit" ></textarea>
    </label>
</form>
<?php
$comments = json_decode(file_get_contents('comments.json'));
    if(isset($_POST['sub'])){
        $name = $_POST["name"];
        $comm = $_POST["commit"];
        $comments[] = ["name" => $name, "comment" => $comm];
        file_put_contents('comments.json',json_encode($comments));
    }
$comments = json_decode(file_get_contents('comments.json'));
echo "<table>";
foreach ($comments as $com)
    echo "<tr><td>".$com -> {"name"}."</td><td>".$com -> {"comment"}."</td></tr>";
echo "</table>";
?>
</body>
</html>
