<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task1.4</title>
</head>
<body>
<form method="post" action="Task1_4.php">
    <label>Введіть початкову дату
        <input type="text" name="first_date">
    </label>
    <br>
    <label>Введіть кінцеву дату
        <input type="text" name="second_date">
    </label>
    <br>
    <input type="submit" name="sub" value="Знайти кількість днів">
    <br>
    <label>
        <?php
            if(isset($_POST['sub']))
            {
                $first_date = $_POST['first_date'];
                $second_date = $_POST['second_date'];

                echo round((-strtotime($first_date) + strtotime($second_date))/(60*60*24), 0);
            }
        ?>
    </label>
</form>
</body>
</html>
