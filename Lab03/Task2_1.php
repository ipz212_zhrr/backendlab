<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task2.1</title>
</head>
<body>
    <form method="post" action="Task2_1.php">
        <?php

            $months = ['Січень','Лютий',
                'Березень','Квітень','Травень',
                'Червень','Липень','Серпень',
                'Вересень','Жовтень','Листопад',
                'Грудень'];
            $today = getdate();

            echo '<select name="day">';
            for ($i = 1; $i <= 31; $i++)
            {
                if ($i == $today["mday"]){
                    echo '<option selected>'.$i.'</option>';
                    continue;
                }
                echo '<option>'.$i.'</option>';
            }
            echo '</select>';

            echo '<select name="mon">';
            foreach ($months as $month) {
                if ($month == $months[$today['mon']-1]){
                    echo '<option selected>'.$month.'</option>';
                    continue;
                }
                echo '<option>'.$month.'</option>';
            }
            echo '</select>';

            echo '<select name="year">';
            for ($i = 1940; $i <= 2040; $i++)
            {
                if ($i == $today["year"]){
                    echo '<option selected>'.$i.'</option>';
                    continue;
                }
                echo '<option>'.$i.'</option>';
            }
            echo '</select>';

            if(isset($_POST['sub']))
            {
                $day = $_POST['day'];
                $mon = $_POST['mon'];
                $year = $_POST['year'];

                $mon_num = array_search($mon, $months) + 1;

                if(checkdate($mon_num, $day, $year)){
                    echo 'Дата коректна: '.$day.' '.$mon.' '.$year;
                }else{
                    echo 'Помилка дати';
                }
            }
        ?>
        <input type="submit" name="sub" value="Перевірити дату">
    </form>
</body>
</html>